Source: herb
Section: electronics
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>
DM-Upload-Allowed: yes
Build-Depends: debhelper (>= 7.0.50~), autotools-dev, lesstif2-dev (>= 0.95.0), 
 intltool ( >= 0.40.1), libgtk2.0-dev ( >= 2.12.5), libglib2.0-dev (>= 2.18.0), 
 libpango1.0-dev (>= 1.20.1), libdbus-glib-1-dev, libtool
Standards-Version: 3.8.2
Homepage: http://www.banu.com/herb
Vcs-Git: git://git.debian.org/git/debian-science/packages/herb.git
Vcs-Browser: http://git.debian.org/?p=debian-science/packages/herb.git

Package: herb
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libfontconfig1 (>= 2.2.0), libfreetype6 (>> 2.1.7), libherb (= ${binary:Version}), herb-data
Description: complete set of CAD programs for design of VLSI circuits
 Herb is a complete set of CAD programs and libraries for the specification,
 design and validation of VLSI circuits. It includes a VHDL simulator, logic
 synthesis tools, automatic place and route tools, layout editors, portable
 CMOS standard cell libraries and custom block generators for creating custom
 high performance circuits.
 .
 Herb supports the following file formats:
  * Behavioural: VHDL, VHDL FSM (input-only)
  * Netlists: Alliance, SPICE, EDIF 2.0, VHDL, COMPASS, HILO (output-only) and
    Verilog (output-only)
  * Symbolic layout: Alliance, COMPASS
  * Physical layout: CIF (output-only) and GDSII (output-only)
  .

Package: herb-data
Section: libdevel
Architecture: all
Depends: ${misc:Depends}
Description: complete set of CAD programs for design of VLSI circuits (data files)
 Herb is a complete set of CAD programs and libraries for the specification,
 design and validation of VLSI circuits. It includes a VHDL simulator, logic
 synthesis tools, automatic place and route tools, layout editors, portable
 CMOS standard cell libraries and custom block generators for creating custom
 high performance circuits.
 .
 Herb supports the following file formats:
  * Behavioural: VHDL, VHDL FSM (input-only)
  * Netlists: Alliance, SPICE, EDIF 2.0, VHDL, COMPASS, HILO (output-only) and
    Verilog (output-only)
  * Symbolic layout: Alliance, COMPASS
  * Physical layout: CIF (output-only) and GDSII (output-only)
  .
 This package contains the architectured independent data files.

Package: herb-dev
Section: libdevel
Architecture: any
Depends: libherb (= ${binary:Version}), ${misc:Depends}
Description: complete set of CAD programs for design of VLSI circuits (development files)
 Herb is a complete set of CAD programs and libraries for the specification,
 design and validation of VLSI circuits. It includes a VHDL simulator, logic
 synthesis tools, automatic place and route tools, layout editors, portable
 CMOS standard cell libraries and custom block generators for creating custom
 high performance circuits.
 .
 Herb supports the following file formats:
  * Behavioural: VHDL, VHDL FSM (input-only)
  * Netlists: Alliance, SPICE, EDIF 2.0, VHDL, COMPASS, HILO (output-only) and
    Verilog (output-only)
  * Symbolic layout: Alliance, COMPASS
  * Physical layout: CIF (output-only) and GDSII (output-only)
  .
 This package contains the header files, static libraries, symbolic links and
 documentation that developers using herb will need.

Package: libherb
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: complete set of CAD programs for design of VLSI circuits (shared libraries)
 Herb is a complete set of CAD programs and libraries for the specification,
 design and validation of VLSI circuits. It includes a VHDL simulator, logic
 synthesis tools, automatic place and route tools, layout editors, portable
 CMOS standard cell libraries and custom block generators for creating custom
 high performance circuits.
 .
 Herb supports the following file formats:
  * Behavioural: VHDL, VHDL FSM (input-only)
  * Netlists: Alliance, SPICE, EDIF 2.0, VHDL, COMPASS, HILO (output-only) and
    Verilog (output-only)
  * Symbolic layout: Alliance, COMPASS
  * Physical layout: CIF (output-only) and GDSII (output-only)
  .
 This package contains the shared libraries.
